#include <QtGui>
#include "server.h"

Server::Server(QWidget *parent)
    : QMainWindow(parent)
{
    imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    createActions();
    createMenus();

    video_thread = new VideoThread(this);
    video_thread->setPort(video_portno);
    video_thread->setBufferSize(bufferSize);

    audio_thread = new AudioThread(this);
    audio_thread->setPort(audio_portno);

    connect(video_thread, SIGNAL(receivedPixmap(const QPixmap*)), this, SLOT(updateImageLabelWithPixmap(const QPixmap*)));

    setWindowTitle(tr("Server"));
    resize(width, height);
}

void Server::updateImageLabelWithPixmap(const QPixmap* p) {
    imageLabel->setPixmap(*p);
    imageLabel->show();
}

void Server::startServer() {
    video_thread->start();
  //  audio_thread->start();
}

void Server::createActions()
{
    startServerAction = new QAction(tr("&Start Server"), this);
    startServerAction->setShortcut(tr("Ctrl+S"));
    connect(startServerAction, SIGNAL(triggered()), this, SLOT(startServer()));
}

void Server::createMenus()
{
    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addAction(startServerAction);

    menuBar()->addMenu(fileMenu);
}

Server::~Server()
{

}
