#-------------------------------------------------
#
# Project created by QtCreator 2017-05-16T15:43:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TARGET = Server
TEMPLATE = app


SOURCES += main.cpp\
        server.cpp \
    AudioInterface.cpp \
    VideoThread.cpp \
    AudioThread.cpp

HEADERS  += server.h \
    audioconfig.h \
    AudioInterface.h \
    AudioThread.h \
    VideoThread.h


unix: CONFIG += link_pkgconfig
#unix: PKGCONFIG += opencv
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../usr/lib/i386-linux-gnu/release/ -lasound
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../usr/lib/i386-linux-gnu/debug/ -lasound
else:unix: LIBS += -L$$PWD/../../../../usr/lib/i386-linux-gnu/ -lasound

INCLUDEPATH += $$PWD/../../../../usr/lib/i386-linux-gnu/alsa-lib
DEPENDPATH += $$PWD/../../../../usr/lib/i386-linux-gnu/alsa-lib
