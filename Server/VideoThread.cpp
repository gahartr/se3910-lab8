#include "VideoThread.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <ctype.h>

VideoThread::VideoThread(QObject *parent) : QThread(parent)
{

}

VideoThread::~VideoThread()
{

}

void VideoThread::run()
{
    QTextStream ts(stdout);
    int video_sockfd, video_newsockfd, video_n;
    unsigned int video_clilen;
    buffer_size = 640 * 480 * 3;
    char video_buffer[buffer_size];
    char tempBuffer[buffer_size];
    struct sockaddr_in video_serv_addr, video_cli_addr;

    video_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (video_sockfd < 0) {
        ts << "error: opening video socket" << endl;
    }

    memset((void*) &video_serv_addr, 0, sizeof(video_serv_addr));
    video_serv_addr.sin_family = AF_INET;
    video_serv_addr.sin_addr.s_addr = INADDR_ANY;
    video_serv_addr.sin_port = htons(port);

    if (bind(video_sockfd, (struct sockaddr*) &video_serv_addr, sizeof(video_serv_addr)) < 0) {
        ts << "error: binding video socket" << endl;
    }

    listen(video_sockfd, 5);
    ts << "Listening for video connection" << endl;

    video_clilen = sizeof(video_cli_addr);
    video_newsockfd = accept(video_sockfd, (struct sockaddr*) &video_cli_addr, &video_clilen);
    if (video_newsockfd < 0) {
        ts << "error: accepting video socket" << endl;
    } else {
        ts << "video connection made" << endl;
    }
     QImage *img;
     int flag = 0;
     QPixmap *pmap;
     pmap = new QPixmap(620, 480);
    while (true) {
        memset(&video_buffer[0], 0, sizeof(video_buffer));
        video_n = read(video_newsockfd, video_buffer, sizeof(video_buffer));
        if (video_n < 0) {
            ts << "error: reading from video socket" << endl;
        }
        if(video_n > 0) {
            int bytesReceived = video_n;
            while(bytesReceived < buffer_size) {
                memset(&tempBuffer[0], 0, sizeof(video_buffer));
                video_n = read(video_newsockfd, &tempBuffer, buffer_size);
                if(bytesReceived + video_n > buffer_size) {
                    video_n = buffer_size - bytesReceived;
                }
                memcpy(&video_buffer[0] + bytesReceived, &tempBuffer, video_n);
                bytesReceived += video_n;

            }
        }

        QByteArray qba (&video_buffer[0]);
        //QPixmap pmap;
        pmap->loadFromData(qba, "PNG");
        //img = new QImage(640, 480, "PNG");
        //img->
        //img = new QImage((const uchar*)&video_buffer[0] ,640, 480, 1920, QImage::Format_Indexed8);
        emit receivedPixmap(pmap);
        /*if(flag == 0) {
            flag++;
            QFile file("testimg.png");
            file.open(QIODevice::WriteOnly);
            file.write(qba);
            file.close();

        }*/
    }
}
