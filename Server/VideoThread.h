#ifndef VIDEOTHREAD_H
#define VIDEOTHREAD_H

#include <QtGui>
#include <QObject>

class VideoThread : public QThread
{
    Q_OBJECT

public:
    VideoThread(QObject *parent = 0);
    ~VideoThread();
    void setPort(int port)
    {
        this->port = port;
    }
    void setBufferSize(int size)
    {
        this->buffer_size = size;
    }

signals:
    void receivedPixmap(const QPixmap* p);

protected:
    void run();

private:
    int port;
    int buffer_size;
};

#endif // VIDEOTHREAD_H
