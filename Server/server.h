#ifndef SERVER_H
#define SERVER_H

#include <QMainWindow>
#include "VideoThread.h"
#include "AudioThread.h"

class QLabel;

class Server : public QMainWindow
{
    Q_OBJECT

public:
    Server(QWidget *parent = 0);
    ~Server();

private slots:
    void startServer();
    void updateImageLabelWithPixmap(const QPixmap *p);

private:
    const static int width = 500;
    const static int height = 400;
    const static int bufferSize = 1024;
    const static int audio_portno = 4444;
    const static int video_portno = 4454;
    void createActions();
    void createMenus();

    VideoThread *video_thread;
    AudioThread *audio_thread;

    QLabel *imageLabel;
    QAction *startServerAction;
    QAction *exitAction;
    QMenu *fileMenu;
};

#endif // SERVER_H
