#include "AudioThread.h"
#include "AudioInterface.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <ctype.h>

AudioThread::AudioThread(QObject *parent) : QThread(parent)
{

}

AudioThread::~AudioThread()
{

}

void AudioThread::run()
{
    QTextStream ts(stdout);
    int audio_sockfd, audio_newsockfd, n;
    unsigned int audio_clilen;
    AudioInterface *ai;
    int rc;
    char *audioBuffer;

    char *tempBuffer;
    int audioBufferSize;
    //char audio_buffer[bufferSize]; //TODO want buffer size of audio
    struct sockaddr_in audio_serv_addr, audio_cli_addr;

    ai = new AudioInterface("plughw:0", SAMPLING_RATE, NUMBER_OF_CHANNELS, SND_PCM_STREAM_PLAYBACK);
    ai->open();
    audioBufferSize = ai->getRequiredBufferSize();

    audioBuffer = (char*)malloc(audioBufferSize);
    tempBuffer = (char*)malloc(audioBufferSize);

    // Set up audio server
    audio_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (audio_sockfd < 0)
    {
        ts << "error: opening audio socket" << endl;
    }

    memset((void*) &audio_serv_addr, 0, sizeof(audio_serv_addr));
    audio_serv_addr.sin_family = AF_INET;
    audio_serv_addr.sin_addr.s_addr = INADDR_ANY;
    audio_serv_addr.sin_port = htons(port);

    if (bind(audio_sockfd, (struct sockaddr*) &audio_serv_addr, sizeof(audio_serv_addr)) < 0) {
        ts << "error: binding audio socket" << endl;
    }

    listen(audio_sockfd, 5);
    ts << "Listening for audio connection" << endl;

    audio_clilen = sizeof(audio_cli_addr);
    audio_newsockfd = accept(audio_sockfd, (struct sockaddr*) &audio_cli_addr, &audio_clilen);
    if (audio_newsockfd < 0) {
        ts << "error: accepting audio socket" << endl;
    } else {
        ts << "audio connection made" << endl;
    }

    while(true) {
        memset(audioBuffer, 0, audioBufferSize);
        n = read(audio_newsockfd, audioBuffer, audioBufferSize);
        if (n < 0) {
            ts << "ERROR reading from socket" << endl;
        }

        if(n > 0) {
            int bytesReceived = n;
            while(bytesReceived < audioBufferSize) {
                n = read(audio_newsockfd, tempBuffer, audioBufferSize);

                memcpy(audioBuffer + bytesReceived, tempBuffer, n);

                bytesReceived += n;
            }
            ai->write(audioBuffer, audioBufferSize);
            ts << "Bytes Received: " << bytesReceived << endl;
        }
    }
}
