#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include "AudioInterface.h"
#include "audioconfig.h"


void *audioThread(void *context) {
	printf("Start Audio Thread\n");
	struct audioThreadParams *methodParams = (audioThreadParams*)context;
	AudioInterface *ai;
	int rc;

	ai = new AudioInterface(methodParams->plughw, SAMPLING_RATE, NUMBER_OF_CHANNELS, SND_PCM_STREAM_CAPTURE);
	ai->open();
	bufferSize = ai->getRequiredBufferSize();
	audioBuffer = (char*)malloc(bufferSize);
	do {
		while(dataSent == 0);
	    pthread_mutex_lock(&audioMutex);
	    //printf("Filling audio buffer\n");
		memset(audioBuffer, 0, bufferSize);
		ai->read(audioBuffer);
		dataSent = 0;
	    pthread_mutex_unlock(&audioMutex);

		bytesToCapture -= bufferSize;

	} while (true);

}
