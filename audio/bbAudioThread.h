#ifndef BBAUDIOTHREAD_H_
#define BBAUDIOTHREAD_H_

#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include "AudioInterface.h"
#include "audioconfig.h"

void *audioThread(void *context);

pthread_mutex_t audioMutex;
char* volatile audioBuffer;
int volatile bufferSize;
int volatile dataSent;

struct audioThreadParams {
	char* plughw;
};

#endif BBAUDIOTHREAD_H_
