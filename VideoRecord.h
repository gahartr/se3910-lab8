#include <opencv2/opencv.hpp>
#include <pthread.h>
#include <atomic>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>

using namespace cv;
using namespace std;

typedef void (*processImage)(uchar*);

class VideoRecord {
  bool run;
  int width, height;
  int portno;
  char* hostname;
  int sock_fd;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  processImage callback;
  pthread_t worker_thread;
  private:
    static void* process_thread(void *);
  public:
    VideoRecord(char, int, int, int, processImage);
    void connect();
    void disconnect();
    void start();
    void stop();
};
