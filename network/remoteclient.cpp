#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <ctype.h>

void error(const char *msg)
{
  perror(msg);
  exit(-1);
}

int main(int argc, char* argv[])
{
  int audio_sockfd, video_sockfd, audio_newsockfd, video_newsockfd, audio_portno, video_portno;
  unsigned int audio_clilen, video_clilen;
  char audio_buffer[256];
  char video_buffer[256];
  struct sockaddr_in audio_serv_addr, audio_cli_addr, video_serv_addr, video_cli_addr;
  int audio_n, video_n;
  int index;

  if (argc < 3)
  {
    fprintf(stderr, "usage: %s <audio-port> <video-port>", argv[0]);
    exit(1);
  }


  // Set up audio server
  audio_portno = atoi(argv[1]);  
  audio_sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (audio_sockfd < 0)
  {
    error("error: opening audio socket");
  }

  memset((void*) &audio_serv_addr, 0, sizeof(audio_serv_addr));
  audio_serv_addr.sin_family = AF_INET;
  audio_serv_addr.sin_addr.s_addr = INADDR_ANY;
  audio_serv_addr.sin_port = htons(audio_portno);

  if (bind(audio_sockfd, (struct sockaddr*) &audio_serv_addr, sizeof(audio_serv_addr)) < 0) {
    error("error: binding audio socket");
  }

  
  // Set up video server
  video_portno = atoi(argv[2]);
  video_sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (video_sockfd < 0) {
    error("error: opening video socket");
  }

  memset((void*) &video_serv_addr, 0, sizeof(video_serv_addr));
  video_serv_addr.sin_family = AF_INET;
  video_serv_addr.sin_addr.s_addr = INADDR_ANY;
  video_serv_addr.sin_port = htons(video_portno);

  if (bind(video_sockfd, (struct sockaddr*) &video_serv_addr, sizeof(video_serv_addr)) < 0) {
    error("error: binding video socket");
  }


  // Listen for connections
  listen(audio_sockfd, 5);
  printf("Listening for audio connection\n");
  listen(video_sockfd, 5);
  printf("Listening for video connection\n");

  
  // Accept when connections are made
  audio_clilen = sizeof(audio_cli_addr);
  audio_newsockfd = accept(audio_sockfd, (struct sockaddr*) &audio_cli_addr, &audio_clilen);
  if (audio_newsockfd < 0) {
    error("error: accepting audio socket");
  } else {
    printf("audio connection made\n");
  }
  
  video_clilen = sizeof(video_cli_addr);
  video_newsockfd = accept(video_sockfd, (struct sockaddr*) &video_cli_addr, &video_clilen);
  if (video_newsockfd < 0) {
    error("error: accepting video socket");
  } else {
    printf("video connection made\n");
  }
  
  // Read from sockets
  memset(&audio_buffer[0], 0, sizeof(audio_buffer));
  audio_n = read(audio_newsockfd, audio_buffer, sizeof(audio_buffer));
  if (audio_n < 0) {
    error("error: reading from audio socket");
  }
  printf("audio: bytes read: %i; message: '%s'\n", audio_n, audio_buffer);

  memset(&video_buffer[0], 0, sizeof(video_buffer));
  video_n = read(video_newsockfd, video_buffer, sizeof(video_buffer));
  if (video_n < 0) {
    error("error: reading from video socket");
  }
  printf("video: bytes read: %i; message: '%s'\n", video_n, video_buffer);


  // Close sockets
  close(audio_sockfd);
  close(audio_newsockfd);
  close(video_sockfd);
  close(video_newsockfd);
  
  return 0;
}
