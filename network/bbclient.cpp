#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <iostream>
#include <cstdlib>

using namespace std;

void error(const char *msg)
{
  perror(msg);
  exit(-1);
}

int sendData(int sock_fd, char *bytes) {
  int n = write(sock_fd, bytes, strlen(bytes));
  if (n < 0)
  {
    fprintf(stderr, "error: writing to socket, %i", sock_fd);
    return -1;
  } else {
    return n;
  }
}

int main(int argc, char *argv[])
{
  int audio_sockfd, audio_portno, video_sockfd, video_portno, n;
  struct sockaddr_in audio_serv_addr;
  struct sockaddr_in video_serv_addr;
  struct hostent *audio_server;
  struct hostent *video_server;

  if (argc < 4) {
    fprintf(stderr, "usage: %s <hostname> <audio-port> <video-port>\n", argv[0]);
    exit(0);
  }


  // Connect to audio server
  audio_portno = atoi(argv[2]);
  audio_sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (audio_sockfd < 0)
  {
    error("error: opening audio socket");
  }

  audio_server = gethostbyname(argv[1]);
  if (audio_server == NULL)
  {
    error("error: no such host");
  }

  memset((void*) &audio_serv_addr, 0, sizeof(audio_serv_addr));
  audio_serv_addr.sin_family = AF_INET;
  memcpy((void*) &audio_serv_addr.sin_addr.s_addr, (void*) audio_server->h_addr, audio_server->h_length);
  audio_serv_addr.sin_port = htons(audio_portno);
  if (connect(audio_sockfd, (struct sockaddr*) &audio_serv_addr, sizeof(audio_serv_addr)) < 0)
  {
    error("error: connecting to audio server");
  }


  // Connect to video server
  video_portno = atoi(argv[3]);
  video_sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (video_sockfd < 0)
  {
    error("error: opening video socket");
  }

  video_server = gethostbyname(argv[1]);
  if (video_server == NULL)
  {
    error("error: no such host");
  }

  memset((void*) &video_serv_addr, 0, sizeof(video_serv_addr));
  video_serv_addr.sin_family = AF_INET;
  memcpy((void*) &video_serv_addr.sin_addr.s_addr, (void*) video_server->h_addr, video_server->h_length);
  video_serv_addr.sin_port = htons(video_portno);
  if (connect(video_sockfd, (struct sockaddr*) &video_serv_addr, sizeof(video_serv_addr)) < 0)
  {
    error("error: connecting to video server");
  }


  // Send data to sockets
  char audioString[] = "audioString";
  char videoString[] = "videoString";
  int audioSent = sendData(audio_sockfd, audioString);
  printf("audiosent: %i\n", audioSent);
  int videoSent = sendData(video_sockfd, videoString);
  printf("videosent: %i\n", videoSent);


  // Close sockets
  close(audio_sockfd);
  close(video_sockfd);

  return 0;
}
