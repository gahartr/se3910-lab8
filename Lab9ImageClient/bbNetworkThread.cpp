#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include "bbNetworkThread.h"

void *networkThread(void *context) {
	printf("Start send thread \n");
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

	struct networkThreadParams *methodParams = (networkThreadParams*)context;
    char buffer[256];

    portno = methodParams->portno;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
	{
		printf("ERROR opening socket\n");
	}
    server = gethostbyname(methodParams->ipaddress);
	if (server == NULL) {
		fprintf(stderr,"ERROR, no such host\n");
		exit(0);
	}
	memset((void*)&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	memcpy((void*)&serv_addr.sin_addr.s_addr,(void*)server->h_addr, server->h_length);
	serv_addr.sin_port = htons(portno);
	if (connect(sockfd,(struct sockaddr*)&serv_addr,sizeof(serv_addr)) < 0)
	{
		printf("ERROR connecting\n");
	}
	printf("Connected!!\n");

	int counter = 0;
	n = 1;
	while(n >= 0) {
		while(*(methodParams->dataSent) == 1);
	    pthread_mutex_lock(methodParams->bufferMutex);
	    printf("Sending data: %d\n", counter);
	    counter++;
	    int bytesSent = 0;
	    while(bytesSent < *(methodParams->bufferSize)) {
	    	n = write(sockfd, methodParams->dataBuffer, *(methodParams->bufferSize)); //TODO what if it doesnt send whole buffer
	    	//write(filedesc, audioBuffer, methodParams->bufferSize);
	    	printf("Bytes sent: %d\n", n);
	    	bytesSent += n;
	    }
	    *(methodParams->dataSent) = 1;
	    pthread_mutex_unlock(methodParams->bufferMutex);
	}
}
