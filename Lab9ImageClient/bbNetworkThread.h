#ifndef BBNETWORKTHREAD_H_
#define BBNETWORKTHREAD_H_

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>

void *networkThread(void *context);

//extern char* volatile audioBuffer;
//extern int volatile audioBufferSize;
//extern int volatile audioDataSent;

struct networkThreadParams {
	char* volatile dataBuffer;
	pthread_mutex_t* bufferMutex;
	//pointer to mutex
	volatile int* bufferSize;
	int portno;
	char* ipaddress;
	volatile int* dataSent;
};

#endif
