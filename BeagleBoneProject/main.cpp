#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include "AudioInterface.h"
#include "bbAudioThread.h"
#include "bbVideoThread.h"
#include "audioconfig.h"
#include "bbNetworkThread.h"

// TO COMPILE:   g++ -std=c++11 -O2 `pkg-config --cflags --libs opencv`-lasound -lpthread *.cpp -o bbClient


int main(int argc, char *argv[])
{
	pthread_t audioPThread, videoPThread, audioNetworkThread, videoNetworkThread;
	struct audioThreadParams audioParams;
	struct networkThreadParams videoNetworkParms, audioNetworkParams;
	audioParams.plughw = argv[1];

	if (pthread_create(&audioPThread, NULL, audioThread, (void *)&audioParams) != 0) {
		printf("Uh-oh!\n");
		return -1;
	}

	audioNetworkParams.bufferMutex = &audioMutex;
	audioNetworkParams.bufferSize = &audioBufferSize;
	while(audioBufferSize == 0);
	audioNetworkParams.dataBuffer = audioBuffer;
	audioNetworkParams.ipaddress = argv[2];
	audioNetworkParams.portno = atoi(argv[3]);
	audioNetworkParams.dataSent = &audioDataSent;


	if (pthread_create(&audioNetworkThread, NULL, networkThread, (void *)&audioNetworkParams) != 0) {
		printf("Uh-oh!\n");
		return -1;
	}

	/*if (pthread_create(&videoPThread, NULL, videoThread, (void *)&audioParams) != 0) {
			printf("Uh-oh!\n");
			return -1;
		}*/


	char* b;
	pthread_join(audioPThread, (void**)&b);
	printf("DONE!\n");
}
