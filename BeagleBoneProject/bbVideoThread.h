#ifndef BBVIDEOTHREAD_H_
#define BBVIDEOTHREAD_H_

#include <opencv2/opencv.hpp>
#include <pthread.h>
#include <unistd.h>

void *videoThread(void *context);

extern pthread_mutex_t videoMutex;
extern char* volatile videoBuffer;
extern int volatile videoBufferSize;
//int captureImageWidth;
//int captureImageHeight;

#endif
