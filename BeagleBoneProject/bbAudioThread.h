#ifndef BBAUDIOTHREAD_H_
#define BBAUDIOTHREAD_H_

#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include "AudioInterface.h"
#include "audioconfig.h"

void *audioThread(void *context);

extern pthread_mutex_t audioMutex;
extern char* volatile audioBuffer;
extern int volatile audioBufferSize;
extern int volatile audioDataSent;

struct audioThreadParams {
	char* plughw;
};

#endif
