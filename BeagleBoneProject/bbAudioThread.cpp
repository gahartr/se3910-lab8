#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include "AudioInterface.h"
#include "audioconfig.h"

#include "bbAudioThread.h"

pthread_mutex_t audioMutex;
char* volatile audioBuffer;
int volatile audioBufferSize;
int volatile audioDataSent;

void *audioThread(void *context) {
	printf("Start Audio Thread\n");
	struct audioThreadParams *methodParams = (audioThreadParams*)context;
	AudioInterface *ai;

	ai = new AudioInterface(methodParams->plughw, SAMPLING_RATE, NUMBER_OF_CHANNELS, SND_PCM_STREAM_CAPTURE);
	ai->open();
	audioBufferSize = ai->getRequiredBufferSize();
	audioBuffer = (char*)malloc(audioBufferSize);
	do {
		while(audioDataSent == 0);
	  pthread_mutex_lock(&audioMutex);
	  //printf("Filling audio buffer\n");
		memset(audioBuffer, 0, audioBufferSize);
		ai->read(audioBuffer);
		audioDataSent = 0;
	  pthread_mutex_unlock(&audioMutex);

	} while (true);

}
