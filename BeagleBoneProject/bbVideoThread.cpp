#include <opencv2/opencv.hpp>
#include <thread>
#include <atomic>
#include <unistd.h>
#include "bbVideoThread.h"
using namespace cv;
using namespace std;

pthread_mutex_t videoMutex;

void *videoThread(void *context){
  VideoCapture capture(0);
  Mat frame;
  while(true){
    capture >> frame;
    if(frame.empty()){
      //cout << "Failed to capture an image" << endl;
    }else{
	    pthread_mutex_lock(&videoMutex);
		Mat _tmp;
		cvtColor(frame, _tmp, CV_BGR2RGB);
	  pthread_mutex_unlock(&videoMutex);
    }
  }
}
