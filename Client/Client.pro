#-------------------------------------------------
#
# Project created by QtCreator 2017-05-16T15:43:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TARGET = Client
TEMPLATE = app


SOURCES += main.cpp\
        client.cpp \
    AudioThread.cpp \
    VideoThread.cpp \
    AudioInterface.cpp

HEADERS  += client.h \
    audioconfig.h \
    AudioInterface.h \
    AudioThread.h \
    VideoThread.h


unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += opencv
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../usr/lib/arm-linux-gnueabihf/release/ -lasound
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../usr/lib/arm-linux-gnueabihf/debug/ -lasound
else:unix: LIBS += -L$$PWD/../../../../usr/lib/arm-linux-gnueabihf/ -lasound

INCLUDEPATH += $$PWD/../../../../usr/lib/arm-linux-gnueabihf
DEPENDPATH += $$PWD/../../../../usr/lib/arm-linux-gnueabihf
