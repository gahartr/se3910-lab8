#include "AudioThread.h"
#include "AudioInterface.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <iostream>
#include <cstdlib>

AudioThread::AudioThread(QObject *parent) : QThread(parent)
{

}

AudioThread::~AudioThread()
{

}

void AudioThread::run()
{
    QTextStream ts(stdout);
    int audio_sockfd;
    struct sockaddr_in audio_serv_addr;
    struct hostent *audio_server;

    audio_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (audio_sockfd < 0) {
        ts << "error: opening audio socket" << endl;
    }

    audio_server = gethostbyname(hostname);
    if (audio_server == NULL) {
        ts << "error: no such host" << endl;
    }

    memset((void*) &audio_serv_addr, 0, sizeof(audio_serv_addr));
    audio_serv_addr.sin_family = AF_INET;
    memcpy((void*) &audio_serv_addr.sin_addr.s_addr, (void*) audio_server->h_addr, audio_server->h_length);
    audio_serv_addr.sin_port = htons(port);
    if (::connect(audio_sockfd, (struct sockaddr*) &audio_serv_addr, sizeof(audio_serv_addr)) < 0)
    {
        ts << "error: connecting to audio server" << endl;
    }

    AudioInterface *ai;
    int rc;
    int bufferSize;
    char* audioBuffer;
	ts << "Creating audio interface" << endl;
    ai = new AudioInterface("plughw:1", SAMPLING_RATE, NUMBER_OF_CHANNELS, SND_PCM_STREAM_CAPTURE);
    ai->open();
    bufferSize = ai->getRequiredBufferSize();
    audioBuffer = (char*)malloc(bufferSize);
    while(true) {
        memset(audioBuffer, 0, bufferSize);
        ts << "Read Buffer" << endl;
        ai->read(audioBuffer);
        ts << "Write Buffer" << endl;
        int n = write(audio_sockfd, audioBuffer, bufferSize);
        if(n < bufferSize) {
            int bytesSent = n;
            while(bytesSent < bufferSize) {
                bytesSent += write(audio_sockfd, audioBuffer + bytesSent, bufferSize - bytesSent);
            }
        }
        ts << "Bytes Sent: " << n << endl;
    }
}

