#ifndef CLIENT_H
#define CLIENT_H

#include <QMainWindow>
#include <QObject>
#include "VideoThread.h"
#include "AudioThread.h"


class QLabel;

class Client : public QMainWindow
{
    Q_OBJECT

public:
    Client(QWidget *parent = 0);
    ~Client();

private slots:
    void connectToServer();
    void updateImageLabelWithPixmap(const QImage *p);

private:
    const static int width = 640;
    const static int height = 480;
    const static int audioBufferSize = 1024;
    const static int videoBufferSize = 1024;
    const static int audio_portno = 4444;
    const static int video_portno = 4454;
    void createActions();
    void createMenus();

    VideoThread *video_thread;
    AudioThread *audio_thread;

    QLabel *imageLabel;
    QAction *connectToServerAction;
    QAction *exitAction;
    QMenu *fileMenu;
};

#endif // CLIENT_H
