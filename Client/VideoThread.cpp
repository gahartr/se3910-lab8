#include "VideoThread.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <atomic>
#include <opencv2/opencv.hpp>

using namespace cv;

VideoThread::VideoThread(QObject *parent) : QThread(parent)
{

}

VideoThread::~VideoThread()
{

}

void VideoThread::run()
{
    QTextStream ts(stdout);

    int video_sockfd;
    struct sockaddr_in video_serv_addr;
    struct hostent *video_server;

    video_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (video_sockfd < 0) {
        ts << "error: opening video socket" << endl;
    }

    video_server = gethostbyname(hostname);
    if (video_server == NULL) {
        ts << "error: no such host" << endl;
    }

    memset((void*) &video_serv_addr, 0, sizeof(video_serv_addr));
    video_serv_addr.sin_family = AF_INET;
    memcpy((void*) &video_serv_addr.sin_addr.s_addr, (void*) video_server->h_addr, video_server->h_length);
    video_serv_addr.sin_port = htons(port);
    if (::connect(video_sockfd, (struct sockaddr*) &video_serv_addr, sizeof(video_serv_addr)) < 0) {
        ts << "error: connecting to video server" << endl;
    }

    VideoCapture capture(0);
    capture.set(CV_CAP_PROP_FRAME_WIDTH,640);   // width pixels
    capture.set(CV_CAP_PROP_FRAME_HEIGHT,480);   // height pixels
    capture.set(CV_CAP_PROP_GAIN, 0);            // Enable auto gain etc.
    if(!capture.isOpened()){   // connect to the camera
       ts << "Failed to connect to the camera." << endl;
    }
	int flag = 0;
    Mat frame;
    QImage *img;
    while(true){
	ts << "capture image" << endl;
      capture >> frame;
      if(frame.empty()){
        ts << "Failed to capture an image" << endl;
      }else{
        Mat _tmp;
        cvtColor(frame, _tmp, CV_BGR2RGB);
        img = new QImage((const uchar *) _tmp.data, _tmp.cols, _tmp.rows, _tmp.step, QImage::Format_RGB888);
        img->bits();
        emit loadedPixmap(img);
        QByteArray qba (921600, 0);
        QBuffer buffer(&qba);
        buffer.open(QIODevice::WriteOnly);
        img->save(&buffer, "PNG");
        int n = write(video_sockfd, qba.data(), qba.count());
        ts << "data sent: " << n << endl;
        if(qba.count() > n ) {
            int bytesSent = n;
            while(bytesSent < qba.count()) {
                n = write(video_sockfd, &qba + bytesSent, qba.count() - bytesSent);
                bytesSent += n;
            }
        }
        if (n < 0) {
            ts << "error: didn't send video bytes" << endl;
        }
      }
    }
}

