#ifndef AUDIOTHREAD_H
#define AUDIOTHREAD_H

#include <QtGui>
#include <QObject>

class AudioThread : public QThread
{
    Q_OBJECT

public:
    AudioThread(QObject *parent = 0);
    ~AudioThread();
    void setPort(int port)
    {
        this->port = port;
    }
    void setBufferSize(int size)
    {
        this->buffer_size = size;
    }
    void setHostname(const char* name)
    {
        this->hostname = name;
    }

protected:
    void run();

 private:
    int port;
    int buffer_size;
    const char* hostname;
};

#endif // AUDIOTHREAD_H
