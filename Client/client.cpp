#include <QtGui>
#include "client.h"

using namespace std;

Client::Client(QWidget *parent)
    : QMainWindow(parent)
{
    imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    createActions();
    createMenus();

    video_thread = new VideoThread(this);
    video_thread->setPort(video_portno);
    video_thread->setBufferSize(videoBufferSize);
    video_thread->setHostname("192.168.1.102");

    audio_thread = new AudioThread(this);
    audio_thread->setPort(audio_portno);
    audio_thread->setHostname("192.168.1.102");

    connect(video_thread, SIGNAL(loadedPixmap(const QImage*)), this, SLOT(updateImageLabelWithPixmap(const QImage*)));

    setWindowTitle(tr("Client"));
    resize(680, 400);
}

void Client::updateImageLabelWithPixmap(const QImage* p) {
    imageLabel->setPixmap(QPixmap::fromImage(*p));
    imageLabel->show();
}

void Client::connectToServer() {
    video_thread->start();
 //   audio_thread->start();
}

void Client::createActions() {
    connectToServerAction = new QAction(tr("&Connect to Server"), this);
    connectToServerAction->setShortcut(tr("Ctrl+S"));
    connect(connectToServerAction, SIGNAL(triggered()), this, SLOT(connectToServer()));
}

void Client::createMenus() {
    fileMenu = new QMenu(tr("&File"), this);
    fileMenu->addAction(connectToServerAction);

    menuBar()->addMenu(fileMenu);
}

Client::~Client()
{

}
