#ifndef VIDEOTHREAD_H
#define VIDEOTHREAD_H

#include <QtGui>
#include <QObject>

class VideoThread : public QThread
{
    Q_OBJECT

public:
    VideoThread(QObject *parent = 0);
    ~VideoThread();
    void setPort(int port)
    {
        this->port = port;
    }
    void setBufferSize(int size)
    {
        this->buffer_size = size;
    }
    void setHostname(const char* name)
    {
        this->hostname = name;
    }

signals:
    void loadedPixmap(const QImage* p);

protected:
    void run();

private:
    int port;
    int buffer_size;
    const char* hostname;
};

#endif // VIDEOTHREAD_H
