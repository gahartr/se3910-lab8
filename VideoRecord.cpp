#include "VideoRecord.h"

VideoRecord::VideoRecord (char* host, int port, int x, int y, processImage cback){
  hostname = host;
  portno = port;
  width = x;
  height = y;
  callback = cback;
}

void VideoRecord::start(){
  run = true;
  pthread_create(&worker_thread, 0, &VideoRecord::process_thread, this);
}

void VideoRecord::stop(){
  run = false;
  close(sock_fd);
  pthread_join(worker_thread, 0);
}

void* VideoRecord::process_thread(void *context){
  VideoRecord *vr = (VideoRecord *)context;
  VideoCapture capture(0);
  Mat frame;
  while(vr->run == true){
    capture >> frame;
    if(frame.empty()){
      cout << "Failed to capture an image" << endl;
    }else{
      Mat _tmp;
      cvtColor(frame, _tmp, CV_BGR2RGB);
      vr->callback(_tmp.data);
    }
  }
}

void VideoRecord::connect(){
  sock_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (sock_fd < 0) {
    cout << "Error: opening video socket" << endl;
  }
  server = gethostbyname(hostname);
  if (server == NULL) {
    cout << "Error: no such host" << endl;
  }
  memset((void*) &serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  memcpy((void*) &serv_addr.sin_addr.s_addr, (void*) server->h_addr, server->h_length);
  serv_addr.sin_port = htons(portno);
  if (connect(sock_fd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0) {
    error("Error: connection to video server");
  }
}

void VideoRecord::disconnect() {
  close(sock_fd);
}

void procImage(uchar* data){
  printf("Process Image\n");
  int n = write(sock_fd, data, sizeof(data));
  if (n < 0)
  {
    fprintf(stderr, "error: writing to socket, %i", sock_fd);
    return -1;
  } else {
    return n;
  }
}

int main(){
  const char* hn = "127.0.0.1";
  VideoRecord vr (hn, 4444, 10, 10, &procImage);
  vr.connect();
  vr.start();
  usleep(1000000);
  vr.stop();
  vr.disconnect();
}
